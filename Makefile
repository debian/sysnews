# Makefile for news	int@link.xs4all.nl
#
# $Revision: 1.6 $
#

VERSION = 0.9

# system news location
NEWSDIR =/var/sysnews
# file in $HOME which indicates the last time news was read
NEWSTIME=.news_time

CC	= gcc
DEFS	= -DNEWSDIR=\"$(NEWSDIR)\" -DNEWSTIME=\"$(NEWSTIME)\" -DVERSION=\"$(VERSION)\"
CFLAGS	= -Wall -O2
LDFLAGS = -s

PREFIX	= /usr/local


OBJECTS = news.o getopt.o

%.o: %.c Makefile
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<

all:	 news manpage

news:	 $(OBJECTS)
	 $(CC) $(LDFLAGS) -o $@ $(OBJECTS)

manpage: news.man Makefile
	 sed -e s,/var/sysnews,$(NEWSDIR),g\
	     -e s,.news_time,$(NEWSTIME),g < news.man > news.1

install: all
	 install -o bin -g bin -m 555 news $(PREFIX)/bin
	 install -m 444 news.1 $(PREFIX)/man/man1

release:
	 tar -cvzf news-$(VERSION).tgz \
	  Makefile README getopt.c getopt.h news.c news.h news.man

clean:
	rm -f news $(OBJECTS) news.1
